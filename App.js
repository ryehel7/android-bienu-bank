import React from 'react'
import { createAppContainer } from 'react-navigation'
import { createStackNavigator } from 'react-navigation-stack'
import Login from './src/components/Login'
import HomeScreen from './src/components/Home'
import SplashScreen from './src/components/SplashScreen'
import { Root } from 'native-base'
import PayScreen from './src/components/Pay'
import ProfileScreen from './src/components/Profile'
import ParkingPayment from './src/components/pay/ParkingPayment'
import { combineReducers, createStore } from 'redux'
import { userActiveReducer } from './src/reducers/user'
import { Provider } from 'react-redux'

const MainNavigator = createStackNavigator({
  SplashScreen: {
    screen: SplashScreen,
    navigationOptions: {
      header: null,
    }
  },

  Login: {
    screen: Login,
    navigationOptions: {
      header: null,
    }
  },

  Main: {
    screen: HomeScreen,
    navigationOptions: {
      header: null
    }
  },

  Profile: {
    screen: ProfileScreen,
    navigationOptions: {
      header: null
    }
  },

  Pay: {
    screen: PayScreen,
    navigationOptions: {
      header: null
    }
  },


  ParkingPayment: {
    screen: ParkingPayment,
    navigationOptions: {
      header: null
    }
  }
},

  {
    initialRouteName: 'SplashScreen',
  }
);

const appReducer = combineReducers({
  userActive: userActiveReducer
});

const rootReducer = (state, action) => {
  if (action.type === 'LOGOUT') {
    console.log('sampe')
    state = undefined;
  }
  return appReducer(state, action);
};

const store = createStore(rootReducer);

const AppNavigator = createAppContainer(MainNavigator);

export default () => (
  <Provider store={store}>
    <Root>
      <AppNavigator />
    </Root>
  </Provider>
)