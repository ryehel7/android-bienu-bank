import API from '../api'

export function doAuth(userName, userPassword) {
    return API.post('/auth',
        { userName: userName, userPassword: userPassword }
    );
}