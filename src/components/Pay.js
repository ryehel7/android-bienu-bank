import React from "react"
import { Text, View, Header, Container, Content, Button, Icon } from "native-base"
import FooterSegment from './Footer'
import { StyleSheet } from 'react-native'

export default class PayScreen extends React.Component {

    doParkingPay = () => {
        this.props.navigation.navigate('ParkingPayment')
    }

    render() {
        return (
            <Container>
                <Header style={{ backgroundColor: '#0093D3' }} />
                <Content padder>
                    <Text style={styles.pageHeader}>Pay / Topup</Text>
                    <View style={{ flexDirection: 'row', flexWrap: 'wrap', flex: 1 }}>
                        <Button transparent style={styles.buttonService}>
                            <Icon name="battery-charging" />
                            <Text>Listrik</Text>
                        </Button>
                        <Button transparent style={styles.buttonService}>
                            <Icon name="card" />
                            <Text>Topup</Text>
                        </Button>
                        <Button transparent style={styles.buttonService} onPress={this.doParkingPay}>
                            <Icon name="subway" />
                            <Text>Parking</Text>
                        </Button>
                    </View>
                </Content>
                <FooterSegment />
            </Container>

        )
    }
}

const styles = StyleSheet.create({
    pageHeader: {
        color: '#CC016B',
        fontWeight: 'bold',
        margin: 15
    },

    buttonService: {
        flexDirection: 'column',
        margin: 10
    },
});