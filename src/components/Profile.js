import React from "react"
import { Text, View, Header, Button, CardItem, Card, Row, Grid, Icon, Container, Content } from "native-base"
import FooterSegment from './Footer'
import { connect } from 'react-redux'
import { StyleSheet } from 'react-native'
import { logout } from '../actions/user'

class ProfileScreen extends React.Component {

    doLogout = async () => {
        await this.props.logout();
        await this.props.navigation.navigate('Login')
    }

    render() {
        return (
            <Container>
                <Header style={{ backgroundColor: '#0093D3' }}
                />
                <Content padder>
                    <View style={{ flex: 1 }}>
                        <Text style={{ textAlign: 'center' }}>This is Profile !</Text>
                    </View>
                    <Card>
                        <CardItem>
                            <Grid>
                                <Row>
                                    <Text>Name    : {this.props.userActive.userFullname} </Text>
                                </Row>
                                <Row>
                                    <Text>Address : {this.props.userActive.userAddress}</Text>
                                </Row>
                            </Grid>

                        </CardItem>
                    </Card>

                    <Button rounded warning style={styles.buttonService} onPress={this.doLogout}>
                        <Text> Logout </Text>
                    </Button>
                </Content>
                <FooterSegment />
            </Container>

        )
    }
}


const styles = StyleSheet.create({
    pageHeader: {
        color: '#CC016B',
        fontWeight: 'bold',
        margin: 15
    },

    buttonService: {
        flexDirection: 'column',
        margin: 10
    },
});

const mapStateToProps = (state) => {
    return {
        userActive: state.userActive
    };
}

const mapDispatchToProps = {
    logout: logout
};

export default connect(mapStateToProps, mapDispatchToProps)(ProfileScreen);