import React from "react"
import { StyleSheet, BackHandler } from 'react-native'
import { Grid, Row, Col, Text, View, Thumbnail, Form, Input, Item, Label, Button, Toast, Spinner } from "native-base"
import * as Font from "expo-font"
import { doAuth } from "../api/user"
import { authUser } from "../actions/user"
import { connect } from "react-redux"

class Login extends React.Component {
    state = { userName: 'ryehel7', userPin: '', loading: true };

    componentDidMount() {
        this.backHandler = BackHandler.addEventListener(
            'hardwareBackPress', () => {
                return true;
            }
        )
    }

    async componentWillMount() {
        await Font.loadAsync({
            Roboto: require('../../node_modules/native-base/Fonts/Roboto.ttf'),
            Roboto_medium: require('../../node_modules/native-base/Fonts/Roboto_medium.ttf')
        });
        this.setState({ loading: false });

    }

    doLogin = async () => {
        await this.setState({ loading: true });
        await doAuth(this.state.userName, this.state.userPin).then((res) => {
            console.log(res.data[0].celengan.deposite);


            if (res.status === 200) {
                //this.props.navigation.navigate('Main', { userFullname: res.data[0].userInfo.fullname })
                this.props.authUser({
                    userFullname: res.data[0].userInfo.fullname,
                    userAddress: res.data[0].userInfo.address,
                    celenganCode: res.data[0].celengan.celenganCode,
                    celenganSaving: res.data[0].celengan.saving,
                    celenganLoan: res.data[0].celengan.loan,
                    celenganDeposite: res.data[0].celengan.deposite,
                })
                this.props.navigation.navigate('Main')
            }
            else {
                console.log("Error !");
            }
        }).catch(err => {
            Toast.show({
                text: "Login Failed ! \n with error " + err.message
            })
        }).finally(() => {
            this.setState({ loading: false })
        })
    };

    render() {
        const uri = "https://icon-library.net/images/money-icon-flat/money-icon-flat-12.jpg";

        if (this.state.loading) {
            return (
                <View style={{ flex: 1, justifyContent: 'center' }}>
                    <Spinner />
                </View>
            )
        }
        else {
            console.log(this.state.userPin)

            return (
                <View style={styles.background} >
                    <Grid>
                        <Row size={1} />
                        <Row size={1} style={{
                            flexDirection: 'column',
                            padding: 30,
                            width: '100%'
                        }}>
                            <Grid>
                                <Col size={4}>
                                    <Text style={styles.loginTextHeader}>PIN Login</Text>
                                </Col>

                                <Col size={1}>
                                    <Thumbnail small source={{ uri: uri }} />
                                </Col>
                            </Grid>

                            <Form>
                                <Item stackedLabel>
                                    <Label style={styles.loginText}>Enter Your 6 digits PIN</Label>
                                    <Input
                                        onChangeText={(userPin) => this.setState({ userPin: userPin })}
                                        secureTextEntry keyboardType="numeric"
                                    />

                                </Item>
                            </Form>
                        </Row>

                        <Row size={2} style={{
                            flexDirection: 'column', padding: 60, width: '100%'
                        }}>

                            <Button style={styles.buttonLogin} full rounded onPress={this.doLogin}>
                                <Text style={styles.buttonLoginText}>
                                    Login
                            </Text>
                            </Button>

                        </Row>
                    </Grid>
                </View>
            )

        }

    }
}

const styles = StyleSheet.create({
    background: {
        flex: 1,
        backgroundColor: '#0093D3',
    },
    loginText: {
        color: '#F5F5F5'
    },
    loginTextHeader: {
        color: '#F5F5F5',
    },
    buttonLogin: {
        backgroundColor: '#CC016B',
    },
    buttonLoginText: {
        color: '#F5F5F5',
    }
})

const mapDispatchToProps = {
    authUser: authUser
};

export default connect(null, mapDispatchToProps)(Login);