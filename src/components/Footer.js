import React from "react"
import { withNavigation } from "react-navigation"
import { Footer, FooterTab, Icon, Button, Text } from "native-base"
import { StyleSheet } from 'react-native'

class FooterSegment extends React.Component {
    render() {

        return (
            <Footer >
                <FooterTab style={styles.foot}>
                    <Button onPress={() => {
                        this.props.navigation.navigate('Main')
                    }}>
                        <Icon name="home" />
                        <Text>Home</Text>
                    </Button>

                    <Button onPress={() => {
                        this.props.navigation.navigate('Pay')
                    }}>
                        <Icon name="journal" />
                        <Text>Pay</Text>
                    </Button>

                    <Button onPress={() => {
                        this.props.navigation.navigate('Profile')
                    }}>
                        <Icon name="person" />
                        <Text>Profile</Text>
                    </Button>

                </FooterTab>
            </Footer>
        )
    }
}

const styles = StyleSheet.create({
    foot: {
        backgroundColor: '#0093D3'
    }
})

export default withNavigation(FooterSegment)