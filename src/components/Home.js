import React from "react"
import { StyleSheet, Image } from 'react-native'
import { Text, View, CardItem, Card, Content, Grid, Icon, ListItem, Row, Container, Header } from "native-base"
import FooterSegment from './Footer'
import { connect } from 'react-redux'

class HomeScreen extends React.Component {
    state = { isShowDeatil: false, iconName: 'arrow-dropdown', loan: '', deposite: '' }


    doShowDetail = () => {
        if (this.state.isShowDeatil) {
            return (
                <Row style={{ height: 'auto', textAlign: 'center' }}>
                    <View style={{
                        padding: 10,
                        justifyContent: "space-between",

                    }}>

                        <ListItem>
                            <Icon name="bookmarks" />
                            <Text>   Saving. Rp. {this.props.userActive.celenganSaving}</Text>

                        </ListItem>

                        <ListItem>
                            <Icon name="alarm" />
                            <Text>   Loan. Rp. {this.props.userActive.celenganLoan}</Text>
                        </ListItem>

                        <ListItem>
                            <Icon name="flame" />
                            <Text>   Deposite. Rp. {this.props.userActive.celenganDeposite}</Text>
                        </ListItem>
                    </View>
                </Row>
            )
        }
        else {
            return null;
        }
    }

    render() {
        return (
            <Container>
                <Header style={{ backgroundColor: '#0093D3' }} />
                <Content padder>
                    <Card>
                        <CardItem header>
                            <Grid>
                                <Row style={{ height: 50 }}>
                                    <View style={{ flex: 1 }}>
                                        <Text style={{ textAlign: 'center' }}>Hi.. {this.props.userActive.userFullname} {this.state.saving}</Text>
                                    </View>
                                </Row>
                                {this.doShowDetail()}
                            </Grid>
                        </CardItem>
                        <CardItem footer button onPress={() => {
                            if (this.state.isShowDeatil) {
                                this.setState({ isShowDeatil: !this.state.isShowDeatil, iconName: 'arrow-dropdown' })
                            }
                            else {
                                this.setState({ isShowDeatil: !this.state.isShowDeatil, iconName: 'arrow-dropup' })
                            }
                        }}>
                            <View style={{ flex: 1 }}>
                                <Text style={{ textAlign: 'center' }}>
                                    <Icon name={this.state.iconName}></Icon>
                                </Text>
                            </View>
                        </CardItem>
                    </Card>

                </Content>
                <FooterSegment />
            </Container>

        )
    }
}

const mapStateToProps = (state) => {
    return {
        userActive: state.userActive
    };
}

export default connect(mapStateToProps)(HomeScreen);