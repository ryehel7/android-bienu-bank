import React from 'react'
import { StyleSheet, Image } from 'react-native';
import { View } from "native-base"

export default class SplashScreen extends React.Component {
    doRedirect = () => {
        setTimeout(() => {
            this.props.navigation.navigate('Login')
        }, 2500)
    };

    render() {
        return (
            <View style={styles.container}>
                <Image style={styles.imageWelcome} source={require('../../assets/Bienu-Bank.png')} />
                {this.doRedirect()}
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#FFFFFF'
    },
    imageWelcome: {
        height: '50%',
        resizeMode: 'center'
    }
})