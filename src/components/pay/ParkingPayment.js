import React from 'react'
import * as Permissions from 'expo-permissions'
import { BarCodeScanner } from 'expo-barcode-scanner'
import { Text, View, Button, Content, Container, Header } from "native-base"
import { StyleSheet, Vibration } from 'react-native'
import FooterSegment from '../Footer'


export default class ParkingPayment extends React.Component {
    state = {
        hasCameraPermission: null,
        scanned: false
    }

    async componentDidMount() {
        this.getPermission();
    }

    getPermission = async () => {
        const { status } = await Permissions.askAsync(Permissions.CAMERA);
        this.setState({ hasCameraPermission: status === 'granted' })
    };

    handleBarcodeScanned = ({ type, data }) => {
        Vibration.vibrate();
        this.setState({ scanned: true });
        alert(`Barcode with type ${type} and data ${data} has been scanned`)
    };

    resetScanner() {
        this.setState({ scanned: false })
    }

    render() {
        return (
            <Container>
                <Header style={{ backgroundColor: '#0093D3' }} />
                <Content padder>
                    <BarCodeScanner onBarCodeScanned={this.state.scanned ? undefined : this.handleBarcodeScanned}
                        style={styles.cameraView}
                    />
                    <View
                        style={{ marginTop: 10 }}
                    >
                        <Button style={styles.buttonScan} full rounded
                            onPress={() => this.setState({ scanned: false })}>
                            <Text>Scan Again</Text>
                        </Button>
                    </View>
                </Content>
                <FooterSegment />
            </Container>
        )
    }
}

const styles = StyleSheet.create({
    buttonScan: {
        backgroundColor: '#CC016B',
        width: 200,
        marginLeft: 'auto',
        marginRight: 'auto'
    },

    cameraView: {
        height: 510,
        width: '100%'
    }
});